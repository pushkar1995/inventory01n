import React, { Component } from 'react';

// import './App.css';
import Toolbar from './components/navigations/toolbar/Toolbar';
import SideDrawer from './components/navigations/sideDrawer/SideDrawer';
import Backdrop from './components/backdrop/Backdrop';

class App extends Component {
  state = {
    sideDrawerOpen: false,
  }

  drawerToogleClickHandler = () => {
    this.setState(prevState => {
      return { sideDrawerOpen: !prevState.sideDrawerOpen}
    })
  }

  backdropClickHandler = () => {
    this.setState({ sideDrawerOpen: false })
  }

  render() {
    let backdrop;

    if (this.state.sideDrawerOpen) {
      backdrop = <Backdrop click={this.backdropClickHandler} />
     
    }
    return (
    <div style={{height: '100%'}}>
      <Toolbar drawerClickHandler={this.drawerToggleClickHandler} />
      <SideDrawer show={this.state.sideDrawerOpen} />
      {backdrop}
      <main style={{ marginTop: '64px' }}>
          <p>This is the page content!</p>
      </main>
    </div>
    )
  }
}

export default App;
